# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help: ## This help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

null:
	help

.PHONY: build
build: ## Building the environment
	@echo "Building environment..."
	docker-compose build

.PHONY: quickInstall
quickInstall: ## Install the dependencies without dev dependencies
	@echo "Installing dependencies without dev dependencies..."
	docker run --rm -v $(shell pwd):/app composer update --no-dev
	cd ./wp/content/themes/encora && npm i --only=prod

.PHONY: install
install: ## Install the dependencies
	@echo "Installing dependencies..."
	docker run --rm -v $(shell pwd):/app composer update
	cd ./wp/content/themes/encora && npm i

.PHONY: local
local: ## Start local environment for development
	@echo "Starting local environment..."
	make stop
	make down
	docker-compose -f ./docker-compose.yaml -f ./docker-compose-db.yaml up -d --remove-orphans

.PHONY: start
start: ## Start the environment
	@echo "Starting environment..."
	make build
	make quickInstall
	make restart
	
.PHONY: stop
stop: ## Stopping environment
	@echo "Stopping environment..."
	docker-compose -f ./docker-compose.yaml -f ./docker-compose-db.yaml stop

.PHONY: down
down: ## Take down the environment
	@echo "Taking down environment..."
	docker-compose -f ./docker-compose.yaml -f ./docker-compose-db.yaml down

.PHONY: restart
restart: ## Restart the environment
	@echo "Restarting environment..."
	make stop
	make down
	docker-compose up -d --remove-orphans

.PHONY: remove
remove: ## Remove the environment
	@echo "Removing environment..."
	docker-compose -f ./docker-compose.yaml -f ./docker-compose-db.yaml down --volume