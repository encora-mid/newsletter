# Encora Times

## Prerrequisites
##### Docker
Install Docker for desktop (https://www.docker.com/products/docker-desktop)

##### NVM
Install NVM (https://github.com/nvm-sh/nvm).

##### Composer
Install composer globally on your system (https://getcomposer.org/doc/00-intro.md#globally).

##### Hosts file
If not already, add an entry for the local URL of the website:
`127.0.0.1       local.encoratimes.com`


## Installation
To install this project, start by cloning this repository with `git clone git@gitlab.com:encora-mid/newsletter.git` or https equivalent `git clone https://gitlab.com/encora-mid/newsletter.git` if SSH is not setup on your account.

##### First time run
The first time you run this project, after the project is running from previous steps, do the following from the terminal on the project directory:
1. Connect to our VPN
2. CD into the project folder with `cd <path-where-you-cloned-the-project>` or directly open a terminal in it
3. Run `nvm install lts/erbium`
4. Run `nvm use`
5. Run `make build && make local`

You will have to import a copy from prod's DB into your local DB container the first time you run this. 
TODO: Script for automatically installing local DB

##### Usual run
After second run, and unless you remove the volumes, you only need to:
1. CD into the project folder with `cd <path-where-you-cloned-the-project>` or directly open a terminal in it
2. Run `nvm use`
3. Run `make local`

You don't have to be logged in to the VPN to do local dev. You'll need to re do the first time run steps if you remove the container volumes by `docker system prune` or similar method.


## Commands

##### make build
Installs composer dependencies and builds the docker containers. You shouldn't need to run this a lot.

##### make local
Starts project on local environment. You can use this command again to restart your local environment.

##### make start
Starts project on remote environments.

##### make restart
Restart project applying down and up instead of simple docker-compose restart (used on remote environments only).

##### make stop
Stop running containers.

##### make down
Down running containers.

##### make remove
Down running containers and also clean its volumes from the system.

##### make help
Prints the makefile help.


## Important folders

##### ./wp/content/plugins
This is the folder where plugins live.

##### ./wp/content/themes/encora
This is the folder where our custom theme lives.

##### ./wp/content/uploads
This is the folder where the uploads get stored locally.

##### /usr/wordpress/uploads
This is the folder where the uploads get stored remotely.