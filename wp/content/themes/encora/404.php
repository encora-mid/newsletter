<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Encora
 */

get_header();
?>

	<main id="primary" class="site-main">

		<header class="page-header">

			<div class="container">

				<div class="row">

					<div class="col-12">

						<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'encora' ); ?></h1>

					</div>

				</div>

			</div>

		</header><!-- .page-header -->

		<section class="py-5 container error-404 not-found">

			<div class="row">

				<div class="col-12 col-md-8 col-lg-9">

					<div class="page-content">

						<p>
							<?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'encora' ); ?>
						</p>

						<div class="mb-5">
							<?php get_search_form(); ?>
						</div>

						<div class="mb-5">
							<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>
						</div>

						<div class="mb-5 widget widget_categories">

							<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'encora' ); ?></h2>
							<ul>
								<?php
								wp_list_categories(
									array(
										'orderby'    => 'count',
										'order'      => 'DESC',
										'show_count' => 1,
										'title_li'   => '',
										'number'     => 10,
									)
								);
								?>
							</ul>

						</div><!-- .widget -->

						<?php
						the_widget( 'WP_Widget_Archives' );
						the_widget( 'WP_Widget_Tag_Cloud' );
						?>

					</div><!-- .page-content -->

				</div><!-- .col-12.col-md-8.col-lg-9 -->

			</div><!-- .row -->

		</section><!-- .container.error-404 -->

	</main><!-- #main -->

<?php
get_footer();
