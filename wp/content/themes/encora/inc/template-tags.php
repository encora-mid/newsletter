<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Encora
 */

if ( ! function_exists( 'encora_entry_terms' ) ) :
	/**
	 * Prints HTML with the entry terms.
	 */
	function encora_entry_terms() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */

			// Get the category list.
			$categories_list = get_the_category_list( esc_html__( ', ', 'encora' ) );

			// Transform back to array.
			$categories_arr = explode( ', ', $categories_list );

			// Get only first 4.
			$categories_arr = array_slice( $categories_arr, 0, 4 );

			// Set color_classes.
			$color_classes = array(
				'blue',
				'yellow',
				'red',
				'green',
			);

			// Randomize color_classes.
			shuffle( $color_classes );

			// Add classes to categories.
			foreach ( $categories_arr as $index => $category ) {
				$color_class = $color_classes[ $index ];
				$categories_arr[ $index ] = str_replace( '<a', "<a class='$color_class'", $category );
			}

			// Implode back to string.
			$categories_list = implode( ', ', $categories_arr );

			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<div class="cat-links">%1$s</div>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}

			/* translators: used between list items, there is a space after the comma */

			/*
			Commented this out
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'encora' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags.
				printf( '<div class="tags-links">%1$s</div>', $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
			*/
		}
	}
endif;

if ( ! function_exists( 'encora_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function encora_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="updated" datetime="%3$s">%4$s</time><time class="entry-date published" datetime="%1$s">%2$s</time>';
		}

		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'encora' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'encora_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function encora_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'by %s', 'post author', 'encora' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

	}
endif;

if ( ! function_exists( 'encora_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function encora_entry_footer() {
		if ( is_single() ) {
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'encora' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<div class="tags-links">' . __( 'Tags: %1$s', 'encora' ) . '</div>', $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}

		/*
		Commented this out
		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'encora' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);
			echo '</span>';
		}
		*/

		if ( is_single() ) :
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( '[Edit<span class="screen-reader-text">%s</span>]', 'encora' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				),
				'<span class="edit-link">',
				'</span>'
			);
		endif;
	}
endif;

if ( ! function_exists( 'encora_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function encora_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php
				the_post_thumbnail(
					'full',
					array(
						'loading' => 'lazy',
					)
				);
				?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

			<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
				<?php
					the_post_thumbnail(
						'feed',
						array(
							'alt'     => the_title_attribute(
								array(
									'echo' => false,
								)
							),
							'loading' => 'lazy',
						)
					);
				?>
			</a>

			<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'wp_body_open' ) ) :
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
endif;
