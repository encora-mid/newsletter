<?php
/**
 * The template for displaying the searchform
 *
 * Contains the markup of the searchform.
 *
 * @package Encora
 */

?>

<form role="search" method="get" action="<?php echo get_site_url(); ?>">
	<div class="form-group">
		<label for="search-form">Search</label>
		<input type="search" id="search-form" class="form-control" name="s" value="<?php echo esc_attr( get_query_var( 's', '' ) ); ?>" placeholder="Search" required="">
	</div>

	<button type="submit" class="btn btn-primary">Search</button>
</form>
