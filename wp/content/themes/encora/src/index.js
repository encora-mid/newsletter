// Do all pre requisites.
import './_bootstrap';

// Start app.
$(function() {
    const $window = $(window);
    const $masthead = $('#masthead');
    const $navbar = $masthead.find('.navbar-wrapper');
    
    const setNavbarClass = () => {
        if ($window.scrollTop() > 0) {
            $navbar.addClass('compact');
        }
        else {
            $navbar.removeClass('compact');
        }
    }
    setNavbarClass();
    
    $window.scroll(setNavbarClass);
});