<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Encora
 */

get_header();
?>

	<main id="primary" class="site-main">

		<div class="py-5 container">

			<div class="row">

				<div class="col-12 col-md-8 col-lg-9">

					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content', get_post_type() );

						the_post_navigation(
							array(
								'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'encora' ) . '</span> <span class="nav-title">%title</span>',
								'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'encora' ) . '</span> <span class="nav-title">%title</span>',
							)
						);

						// If comments are open or we have at least one comment, load up the comment template.
						/*
						Commented this out
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						*/

					endwhile; // End of the loop.
					?>

				</div><!-- .col-12.col-md-8.col-lg-9 -->

				<div class="col-12 col-md-4 col-lg-3">

					<?php get_sidebar(); ?>

				</div><!-- .col-12.col-md-4.col-lg-3 -->

			</div><!-- .row -->

		</div><!-- .container -->

	</main><!-- #main -->

<?php
get_footer();
