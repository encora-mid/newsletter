<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Encora
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">

				<div class="container">

					<div class="row">

						<div class="col-12">

							<h1 class="page-title">
								<?php
								/* translators: %s: search query. */
								printf( esc_html__( 'Search Results for: %s', 'encora' ), '<span>' . get_search_query() . '</span>' );
								?>
							</h1>

						</div><!-- .col-12 -->

					</div><!-- .row -->

				</div><!-- .container -->

			</header><!-- .page-header -->

			<div class="page-content">

				<div class="py-5 container">

					<div class="row">

						<div class="col-12 col-md-8 col-lg-9">

							<?php
							/* Start the Loop */
							while ( have_posts() ) :
								the_post();

								/**
								 * Run the loop for the search to output the results.
								 * If you want to overload this in a child theme then include a file
								 * called content-search.php and that will be used instead.
								 */
								get_template_part( 'template-parts/content', 'search' );

							endwhile;

							the_posts_navigation();
							?>

						</div>

						<div class="col-12 col-md-4 col-lg-3">

							<?php get_sidebar(); ?>

						</div><!-- .col-12.col-md-4.col-lg-3 -->

					</div>

				</div>

			</div>

			<?php
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();
