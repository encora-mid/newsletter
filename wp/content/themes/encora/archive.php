<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Encora
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">

				<div class="container">

					<div class="row">

						<div class="col-12">

							<?php
							the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
							?>

						</div><!-- .col-12 -->

					</div><!-- .row -->

				</div><!-- .container -->

			</header><!-- .page-header -->

			<div class="py-5 container">

				<div class="row">

					<div class="col-12 col-md-8 col-lg-9">

						<?php
						/* Start the Loop */
						while ( have_posts() ) :
							the_post();

							/*
							* Include the Post-Type-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Type name) and that will be used instead.
							*/
							get_template_part( 'template-parts/content', get_post_type() );

						endwhile;

						/* Print the navigation */
						the_posts_pagination(
							array(
								'prev_text' => '← Previous',
								'next_text' => 'Next →',
							)
						);
						?>

					</div><!-- .col-12.col-md-8.col-lg-9 -->

					<div class="col-12 col-md-4 col-lg-3">

						<?php get_sidebar(); ?>

					</div><!-- .col-12.col-md-4.col-lg-3 -->

				</div><!-- .row -->

			</div><!-- .container -->

		<?php else : ?>

			<header class="container page-header">

				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>

			</header><!-- .page-header -->

			<div class="container">

				<div class="row">

					<div class="col-12 col-md-8 col-lg-9">

						<?php get_template_part( 'template-parts/content', 'none' ); ?>

					</div><!-- .col-12.col-md-8.col-lg-9 -->

					<div class="col-12 col-md-4 col-lg-3">

						<?php get_sidebar(); ?>

					</div><!-- .col-12.col-md-4.col-lg-3 -->

				</div><!-- .row -->

			</div><!-- .container -->

		<?php endif; ?>

	</main><!-- #main -->

<?php
get_footer();
