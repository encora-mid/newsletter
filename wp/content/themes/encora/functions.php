<?php
/**
 * Encora functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Encora
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function encora_setup() {
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// Add 'feed' image size.
	add_image_size( 'feed', 850, 400, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'encora' ),
		)
	);

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action( 'after_setup_theme', 'encora_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function encora_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'encora' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'encora' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'encora_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function encora_scripts() {
	// Get asset manifest.
	$assets = require_once get_stylesheet_directory() . '/build/index.asset.php';

	// Import main stylesheet.
	wp_enqueue_style( 'encore', get_stylesheet_directory_uri() . '/build/index.css', array(), $assets['version'] );

	// Import popper.js.
	wp_enqueue_script( 'popper.js', get_stylesheet_directory_uri() . '/node_modules/popper.js/dist/umd/popper.min.js', array( 'jquery' ), $assets['version'], true );

	// Import bootstrap JS.
	wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.min.js', array( 'jquery', 'popper.js' ), $assets['version'], true );

	// Import main JS with its deps.
	$deps = array_merge(
		$assets['dependencies'],
		array(
			'popper.js',
			'bootstrap',
		)
	);
	wp_enqueue_script( 'encora', get_stylesheet_directory_uri() . '/build/index.js', $deps, $assets['version'], true );

	// Add HRM on local.
	if ( getenv( 'APP_ENV' ) === 'local' ) {
		wp_enqueue_script( 'livereload', get_site_url() . ':35729/livereload.js', array(), $assets['version'], true );
	}
}
add_action( 'wp_enqueue_scripts', 'encora_scripts' );

/**
 * Add scripts after all finishes
 */
add_action(
	'wp_footer',
	function() {
		?>
		<noscript id="deferred-styles">
			<!-- Google Fonts -->
			<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:ital,wght@0,400;0,500;0,600;1,400;1,500;1,700&family=Montserrat:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet"><?php // phpcs:ignore. ?>
			<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@900&display=swap" rel="stylesheet"><?php // phpcs:ignore. ?>

			<!-- Font Awesome -->
			<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"><?php // phpcs:ignore. ?>
		</noscript>
		<script>
			var loadDeferredStyles = function() {
				var addStylesNode = document.getElementById("deferred-styles");
				var replacement = document.createElement("div");
				replacement.innerHTML = addStylesNode.textContent;
				document.body.appendChild(replacement)
				addStylesNode.parentElement.removeChild(addStylesNode);
			};
			var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
			if (raf) raf(function() {
				window.setTimeout(loadDeferredStyles, 0);
			});
			else window.addEventListener('load', loadDeferredStyles);
		</script>
		<?php
	}
);

// Check APP_ENV. Do this only for prod.
if ( getenv( 'APP_ENV' ) === 'prod' ) {

	/**
	 * Helper function to keep buckets updated
	 *
	 * @param Int $post_id the source post ID.
	 */
	function encora_trigger_bucket_sync( $post_id ) {
		// Run script.
		shell_exec( '/var/www/scripts/syncbucket.sh' ); // phpcs:ignore.
	}

	add_filter(
		'wp_generate_attachment_metadata',
		function( $metadata, $attachment_id, $context ) {
			encora_trigger_bucket_sync( $attachment_id );

			return $metadata;
		},
		10,
		3
	);

	add_action(
		'delete_attachment',
		function( $post_id ) {
			encora_trigger_bucket_sync( $post_id );
		}
	);

}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}
