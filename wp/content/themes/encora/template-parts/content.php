<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Encora
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">

		<?php encora_entry_terms(); ?>

		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				encora_posted_on();
				encora_posted_by();
				?>
			</div><!-- .entry-meta -->

		<?php endif; ?>

	</header><!-- .entry-header -->

	<?php encora_post_thumbnail(); ?>

	<div class="entry-content">

		<?php
		if ( is_singular() ) :

			the_content(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'encora' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'encora' ),
					'after'  => '</div>',
				)
			);

		else :
			$excerpt = get_the_excerpt();
			if ( '' !== $excerpt ) :
				?>

				<p>
					<?php echo wp_kses_post( $excerpt ); ?>
				</p>
				<a class="read-more" href="<?php echo esc_attr( get_the_permalink() ); ?>"><?php echo esc_attr( __( 'Continue Reading →', 'encora' ) ); ?></a>

				<?php
			endif;
		endif;
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php encora_entry_footer(); ?>

	</footer><!-- .entry-footer -->

	<!-- .clearfix -->
	<div class="clearfix"></div>

</article><!-- #post-<?php the_ID(); ?> -->
