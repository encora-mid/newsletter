<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Encora
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="shortcut icon" href="<?php echo esc_attr( get_stylesheet_directory_uri() ); ?>/favicon.ico" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'encora' ); ?></a>

	<header id="masthead" class="site-header<?php echo is_front_page() ? esc_attr( ' front-page' ) : ''; ?>">
		<div class="fixed-top shadow-sm">
			<div class="navbar-wrapper<?php echo ! is_front_page() ? esc_attr( ' mono' ) : ''; ?>">
				<div class="container">
					<div class="row">
						<nav class="w-100 navbar navbar-expand-lg navbar-dark">
							<a class="navbar-brand" href="<?php echo esc_attr( get_site_url() ); ?>">
								<img src="<?php echo esc_attr( get_stylesheet_directory_uri() . '/assets/images/encora-logo.svg' ); ?>" />
							</a>
							<button class="btn btn-outline-light d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation">
								<i class="fa fa-bars"></i>
							</button>
							<div class="collapse navbar-collapse" id="collapsibleNavId">
								<?php
								/*
								Commented this out
								<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
									<li class="nav-item">
										<a class="nav-link" href="#">Link</a>
									</li>
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
										<div class="dropdown-menu" aria-labelledby="dropdownId">
											<a class="dropdown-item" href="#">Action 1</a>
											<a class="dropdown-item" href="#">Action 2</a>
										</div>
									</li>
								</ul>
								*/
								?>
								<form action="<?php echo esc_attr( get_site_url() ); ?>" class="form-inline my-2 my-lg-0 ml-auto">
									<input class="form-control mr-sm-2" type="text" name="s" value="<?php echo esc_attr( get_query_var( 's', '' ) ); ?>" placeholder="Search">
									<button class="btn btn-outline-light my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
								</form>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>

		<?php if ( is_front_page() ) : ?>

		<div class="jumbotron jumbotron-fluid h-100 bg-transparent text-white">
			<div class="container d-flex align-items-center flex-wrap align-content-center h-100">
				<h1 class="display-4 text-white w-100"><?php echo esc_attr( get_bloginfo( 'name' ) ); ?></h1>
				<p class="lead w-100"><?php echo esc_attr( get_bloginfo( 'description' ) ); ?></p>
			</div>
		</div>

		<?php endif; ?>

		<?php
		/*
		Commented this out

		<img src="<?php echo esc_url( get_stylesheet_directory_uri() ) . '/assets/images/header.png'; ?>" class="img-fluid w-100" />

		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$encora_description = get_bloginfo( 'description', 'display' );
			if ( $encora_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $encora_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'encora' ); ?></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>
		</nav><!-- #site-navigation -->
		*/
		?>
	</header><!-- #masthead -->
