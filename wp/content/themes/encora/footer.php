<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Encora
 */

?>

	<footer id="colophon" class="text-light site-footer">
		<div class="text-center d-flex align-items-center justify-content-center site-info">
			<p class="mb-0"><?php echo esc_attr( get_bloginfo( 'name' ) ); ?> © <?php echo esc_attr( gmdate( 'Y' ) ); ?></p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
