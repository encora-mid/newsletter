#! /bin/bash

# Rsync to bucket
gsutil -m rsync -rdu -x '^.*node_modules.*$' /var/www/html/wp-content/ gs://prod-assets-bucket/wp-content
gsutil -m rsync -rdu /var/www/html/wp-includes/ gs://prod-assets-bucket/wp-includes