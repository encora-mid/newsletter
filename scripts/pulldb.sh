#! /bin/bash

# Don't run this on prod
APP_ENV=${APP_ENV:-"prod"}
if [ "${APP_ENV}" = "prod" ]; then
    echo "### ENV is PROD! ###";
    exit 0;
fi

# Go through login when doing locally
if [ "${APP_ENV}" = "local" ]; then
    # Login to gcloud
    gcloud auth login

    # Set current project
    gcloud config set project newsletter-283921

    # Connect to DB
    gcloud sql connect newletter-sql --user=encora
fi

# Drop current DB
echo "### Dropping current DB ###";
mysql -h ${WP_DB_HOST} -u ${WP_DB_USER} -p${WP_DB_PASSWORD} -e "DROP DATABASE IF EXISTS `${WP_DB_NAME}`"

# Re create DB
echo "### Re creating DB ###";
mysql -h ${WP_DB_HOST} -u ${WP_DB_USER} -p${WP_DB_PASSWORD} -e "CREATE DATABASE `${WP_DB_NAME}`"

# Import prod DB into local DB
echo "### Importing latest DB from prod ###";
mysqldump -alv -h "${WORDPRESS_SOURCE_DB_HOST}" -u "${WORDPRESS_SOURCE_DB_USER}" -p"${WORDPRESS_SOURCE_DB_PASSWORD}" "${WORDPRESS_SOURCE_DB_NAME}" | mysql -h "${WP_DB_HOST}" -u "${WP_DB_USER}" -p"${WP_DB_PASSWORD}" "${WP_DB_NAME}"

# Replace URLs
wp search-replace "https://encoratimes.com" "${WP_URL}"

# Sync assets with remote bucket
# Rsync from bucket
gsutil -m rsync -rdu gs://prod-assets-bucket/wp-content/uploads/ /var/www/html/wp-content/uploads

# Flush cache
wp cache flush