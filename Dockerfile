FROM wordpress:5.4.2-php7.4-fpm

# Set working directory
WORKDIR /var/www/html

# Set cloud SDK vars
ARG CLOUD_SDK_VERSION=232.0.0
ENV CLOUD_SDK_VERSION=$CLOUD_SDK_VERSION

# Install required libs before extensions
RUN apt-get -qqy update && apt-get install -qqy \
    vim \
    mariadb-client \
    curl \
    gcc \
    python-dev \
    python-setuptools \
    apt-transport-https \
    lsb-release \
    openssh-client \
    git \
    gnupg \
    python3-pip

# Run crcmod install
RUN pip3 install -U crcmod

# Install additional GCloud SDK libs
RUN export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" && \
    echo "deb https://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" > /etc/apt/sources.list.d/google-cloud-sdk.list

# Add key
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

# Install GCloud SDK
RUN apt-get update && \
    apt-get install -y google-cloud-sdk=${CLOUD_SDK_VERSION}-0

# Install WP CLI
# https://make.wordpress.org/cli/2018/05/31/gpg-signature-change/
ENV WORDPRESS_CLI_VERSION 2.4.0
RUN curl -o /usr/local/bin/wp -fSL "https://github.com/wp-cli/wp-cli/releases/download/v${WORDPRESS_CLI_VERSION}/wp-cli-${WORDPRESS_CLI_VERSION}.phar";
RUN chmod +x /usr/local/bin/wp

# Mod www-data user to match gitlab-runner
RUN groupmod -g 993 www-data
RUN usermod -u 997 www-data

# Set directory permissions
RUN chown -R www-data:www-data /var/www/html

# Fix wp-content permissions
RUN chmod -R 755 /var/www/html/wp-content

# Change current user to www
USER www-data

# Expose port 9000
EXPOSE 9000

# Run php-fpm
CMD ["php-fpm"]