<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

/** Enable W3 Total Cache */
define( 'WP_CACHE', true ); // Added by W3 Total Cache.

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv( 'WP_DB_NAME' ) );

/** MySQL database username */
define( 'DB_USER', getenv( 'WP_DB_USER' ) );

/** MySQL database password */
define( 'DB_PASSWORD', getenv( 'WP_DB_PASSWORD' ) );

/** MySQL hostname */
define( 'DB_HOST', getenv( 'WP_DB_HOST' ) );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', '0ac8224d1110add1b65af07ab2c3d3bf05267b0b' );
define( 'SECURE_AUTH_KEY', '301a29ddf170446a9a1f3edd0634251194d57c81' );
define( 'LOGGED_IN_KEY', '6828807f1134dfc7dfbcc9aefaff75af9ccac525' );
define( 'NONCE_KEY', '20e136569636f75f20801395e8b5f5fc27f96e79' );
define( 'AUTH_SALT', '21bc897bda5c44b9bf9cb17dce537df87cf8aea1' );
define( 'SECURE_AUTH_SALT', '7fc9ab0c71615f841f48a14d6e3fa13e10ee6500' );
define( 'LOGGED_IN_SALT', '83c22daffa9ffc0f0be43d76a808f2650c84929f' );
define( 'NONCE_SALT', '5395279b6f08e436b3ec1f16cbc02cc9d1d857f5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = getenv( 'WP_TABLE_PREFIX' );

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', getenv( 'WP_DEBUG' ) );
define( 'WP_DEBUG_LOG', true );
define( 'DISALLOW_FILE_MODS', true );
define( 'DISALLOW_FILE_EDIT', true );

// If we're behind a proxy server and using HTTPS, we need to alert WordPress of that fact.
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy.
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && 'https' === $_SERVER['HTTP_X_FORWARDED_PROTO'] ) {
	$_SERVER['HTTPS'] = 'on';
}

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
